# VSCode 

## Microsoft code-server via web in a Docker Container

This project creates a container that provides Microsoft's code-server which  can be accessed via we web page. This provides the minimal setup accessing a web based VSCode environment. 

## Building test and production images

Commits to the *main* branch trigger a CI/CD pipeline to build a new docker image. You can then deploy the new image for production use by triggering the manual deploy step from this Gitlab project. This will stop and restart all production containers for this image to ensure that all users are running the new version.

When users access the production instances via the cmgr.ncshare.org reservation system they are told how to access their instance via the web or via an ssh command from a terminal session.

Commits to the *staging* branch triggers a CI/CD pipeline and (assuming the image build is successful) will automatically deploy a test instance of the container. 

The web interface to the test container runs at
          https://compute-04.ncshare.org:8443/?ngxtndotoken=<PASSWORD_GOES_HERE>


