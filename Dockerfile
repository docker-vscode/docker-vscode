FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt install -y \
   curl \
   make \
   gcc \
   build-essential \
   curl \
   python \
   sshpass \
   supervisor \
   wget

### text editors ###
RUN DEBIAN_FRONTEND=noninteractive apt install -y \
    nano \
    vim \
    emacs

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
   gcc \
   make \
   python3 \
   zip 

### SETUP jovyan user
RUN useradd -d /home/jovyan -m -s /bin/bash jovyan
RUN echo 'jovyan:jovyan' | chpasswd

############## start code-server #################
ENV VSCODE_HOST="0.0.0.0" \
    VSCODE_PORT=8080 \
    VSCODE_BASE="/opt/pub/apps/vscode-server" \
    VSCODE_PROJECT="${HOME}" \
    VSCODE_LATEST_URL="https://vscodeserverlauncher.blob.core.windows.net/builds/latestbin" \
    VSCODE_TARGET="x86_64-unknown-linux-gnu"

# derived environment vars
ENV VSCODE_SERVER_DATA_DIR="${VSCODE_BASE}/data"

# install & config VS code-server
WORKDIR "${VSCODE_BASE}"
RUN mkdir "bin" "data" && \
        curl -Lo 'bin/code-server' "${VSCODE_LATEST_URL}/${VSCODE_TARGET}/${VSCODE_TARGET}" && \
    chmod a+rx 'bin/code-server' && \
    chown jovyan:jovyan "data"

## switch to user & install any required vs-code extensions
USER jovyan
RUN bin/code-server \
     serve-local --accept-server-license-terms \
              --install-extension "ms-python.python" \
              --install-extension "ms-vscode.makefile-tools" \
              --install-extension "ms-vscode.cpptools" \
              --install-extension "ms-vscode.cpptools-extension-pack"
			  
# vscode puts a bunch of crud in the user's home directory that we don't want to keep in the container
# but we also want the user to have a persistent workspace that is not blown away on container restarts
# so let's make a workspace and volume mount persistent storage for it at runtime
# 
# we will keep the persistent data for the user under the ~/workspace directory
# and cd to that directory for interactive terminal sessions
RUN mkdir /home/jovyan/workspace ; echo 'cd /home/jovyan/workspace' >> /home/jovyan/.bashrc

EXPOSE 8080

# note that we are configured to accept connections from anywhere and have not connection-token (i.e. no authentication)
# the potential security issues of this are handled by launching this container so that the exposed port is only acessible on
# the host server. We do this so the Nginx container can talk to this cotain and proxy for it. Nginx is also configured to handle
# the authentication via proxy auth. Finally note that Nginx has to be configured to both proxy and reverse proxy or the web UI 
# will not work 

# launch MS VScode-server
CMD [ "bash", "-lc", \
        "bin/code-server serve-local --without-connection-token --verbose  --accept-server-license-terms --port ${VSCODE_PORT} --host ${VSCODE_HOST} --telemetry-level off" ]

############## end vccode-server #################


